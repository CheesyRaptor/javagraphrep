package graph;

public class NodeDoesNotExistException extends RuntimeException {

    NodeDoesNotExistException(Integer node) {
         super(node.toString());
    }
    
}
