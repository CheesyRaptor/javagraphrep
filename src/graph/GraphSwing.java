package graph;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingWorker;

public class GraphSwing extends Graph {

	private Map<Integer, Point> layout = new HashMap<>();

	private Set<Integer> selectedNodes = new HashSet<>();

	private int radius = 40;

	private ArrayList<Integer> currentPath = null;

	private ArrayList<Integer> shortestPath = null;

	private javax.swing.JPanel drawingPanel = null;

	private SwingWorker pathFinder = null;

	private boolean findSlowly = false;

	private boolean paused = false;

	private boolean killPathFinding = false;

	public enum SelectionState {
		disconnectingNodes, connectingNodes, relocatingNode, includingNodes, excludingNodes,
		selectingStartNode, selectingFinishNode, none
	};

	private SelectionState selectionState = SelectionState.none;

	private Integer relocatedNode = null;

	GraphManagable frame;

	public void findSlowly() {
		findSlowly = true;
	}

	public void findFast() {
		findSlowly = false;
	}

	public void pausePathFinding() {
		if (pathFinder != null) {
			paused = true;
		}
	}

	public void resumePathFinding() {
		if (pathFinder != null) {
			paused = false;
		}
	}

	public void stopPathFinding() {
		if (pathFinder != null) {
			killPathFinding = true;
			paused = false;
		}
	}

	public void setDrawingPanel(javax.swing.JPanel drawingPanel) {
		this.drawingPanel = drawingPanel;
	}

	public void toggleStartNode(Point pointClicked) {
		Integer nodeClicked = nodeClicked(pointClicked);
		if (nodeClicked != null && !nodeClicked.equals(getFinishNode())) {
			if (nodeClicked.equals(getStartNode())) {
				setStartNode(null);
			} else {
				setStartNode(nodeClicked);
			}
		}
	}

	public void toggleFinishNode(Point pointClicked) {
		Integer nodeClicked = nodeClicked(pointClicked);
		if (nodeClicked != null) {
			if (nodeClicked.equals(getFinishNode())) {
				setFinishNode(null);
			} else {
				setFinishNode(nodeClicked);
			}
		}
	}

	public Integer getRelocatedNode() {
		Integer safeRelocatedNode = relocatedNode;
		return safeRelocatedNode;
	}

	public void setRelocatedNode(Integer relocatedNode) {
		this.relocatedNode = relocatedNode;
	}

	public void releaseRelocatedNode() {
		relocatedNode = null;
	}

	public boolean relocateNode(Point toPoint) {
		if (relocatedNode == null) {
			return false;
		}
		layout.get(relocatedNode).x = toPoint.x;
		layout.get(relocatedNode).y = toPoint.y;
		return true;
	}

	public void resetSelection() {
		selectedNodes = new HashSet<>();
		relocatedNode = null;
	}

	public void setSelectionState(SelectionState state) {
		if (state == selectionState) {
			return;
		}
		selectionState = state;
		resetSelection();
	}

	public SelectionState getSelectionState() {
		SelectionState state = selectionState;
		return state;
	}

	public void setNodeLocation(Integer node, int x, int y) {
		layout.put(node, new Point(x, y));
	}

	public void setNodeLocation(Integer node, Point point) {
		layout.put(node, point);
	}

	public void selectNode(Integer node) {
		selectedNodes.add(node);
	}

	public void deselectNode(Integer node) {
		selectedNodes.remove(node);
	}

	public void toggleIncludedNodeSelection(Point pointClicked) {
		Integer nodeClicked = nodeClicked(pointClicked);
		if (nodeClicked != null) {
			if (super.includedNodes.contains(nodeClicked)) {
				removeFromIncludedNodes(nodeClicked);
			} else {
				addToIncludedNodes(nodeClicked);
			}
		}
	}

	public void toggleExcludedNodeSelection(Point pointClicked) {
		Integer nodeClicked = nodeClicked(pointClicked);
		if (nodeClicked != null) {
			if (excludedNodes.contains(nodeClicked)) {
				removeFromExcludedNodes(nodeClicked);
			} else {
				addToExcludedNodes(nodeClicked);
			}
		}
	}

	public void resetIncludedAndExcludedNodes() {
		includedNodes = new HashSet<>();
		excludedNodes = new HashSet<>();
	}

	public void deselectAllNodes() {
		selectedNodes = new HashSet<>();
	}

	@Override
	public void deleteNode(Integer node) throws NodeDoesNotExistException {
		layout.remove(node);
		selectedNodes.remove(node);
		if (relocatedNode != null && relocatedNode.equals(node)) {
			relocatedNode = null;
		}
		if (includedNodes.contains(node)) {
			includedNodes.remove(node);
		}
		if (excludedNodes.contains(node)) {
			excludedNodes.remove(node);
		}
		if (shortestPath.contains(node)) {
			shortestPath = null;
		}
		super.deleteNode(node);
	}

	@Override
	public void createNode(Integer node) throws CreatingAlreadyExistingNodeException {
		super.createNode(node);
		setNodeLocation(node, 0, 0);
	}

	public void createNode(Integer node, Point point) throws CreatingAlreadyExistingNodeException {
		super.createNode(node);
		setNodeLocation(node, point.x, point.y);
	}

	public void drawGraph(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		Color backgroundColor = g2d.getColor();

		//Connect nodes
		g2d.setColor(Color.LIGHT_GRAY);
		for (Entry<Integer, Point> node : layout.entrySet()) {
			for (Integer connectedNode : super.nodes.get(node.getKey())) {
				g2d.drawLine(node.getValue().x, node.getValue().y,
								layout.get(connectedNode).x, layout.get(connectedNode).y);
			}
		}
		g2d.setStroke(new BasicStroke(2));

		for (Integer node1 : selectedNodes) {
			for (Integer node2 : selectedNodes) {
				switch (selectionState) {
					case disconnectingNodes:
						g2d.setColor(Color.RED);
						if (areConnected(node1, node2)) {
							g2d.drawLine(layout.get(node1).x, layout.get(node1).y, layout.get(node2).x, layout.get(node2).y);
						}
						break;
					case connectingNodes:
						g2d.setColor(Color.GREEN);
						if (!areConnected(node1, node2)) {
							g2d.drawLine(layout.get(node1).x, layout.get(node1).y, layout.get(node2).x, layout.get(node2).y);
						}
						break;
					case none:
						break;
				}
			}
		}
		g2d.setStroke(new BasicStroke(1));

		//Draw currentPath if exists
		if (currentPath != null) {
			drawPath(currentPath, Color.YELLOW, g2d);
		}

		if (shortestPath != null) {
			drawPath(shortestPath, Color.GREEN, g2d);
		}

		//Draw nodes
		for (Entry<Integer, Point> node : layout.entrySet()) {
			// Nodes color
			if (selectedNodes.contains(node.getKey())) {
				g2d.setColor(Color.ORANGE);
			} else if (getStartNode() != null && getStartNode().equals(node.getKey())) {
				g2d.setColor(Color.BLUE);
			} else if (getFinishNode() != null && getFinishNode().equals(node.getKey())) {
				g2d.setColor(Color.CYAN);
			} else if (includedNodes.contains(node.getKey())) {
				g2d.setColor(Color.GREEN);
			} else if (excludedNodes.contains(node.getKey())) {
				g2d.setColor(Color.RED);
			} else {
				g2d.setColor(Color.LIGHT_GRAY);
			}
			// Draw node
			Ellipse2D.Double circle = new Ellipse2D.Double(node.getValue().x - radius / 2,
							node.getValue().y - radius / 2, radius, radius);
			g2d.fill(circle);
			g2d.setColor(Color.BLACK);
			g2d.drawString(node.getKey().toString(), node.getValue().x - 3, node.getValue().y + 3);
		}
		g2d.setColor(backgroundColor);
	}

	public void toggleNodeSelection(Point pointClicked) {
		for (Entry<Integer, Point> node : layout.entrySet()) {
			if (pointClicked.distance(node.getValue()) < radius) {
				if (!selectedNodes.contains(node.getKey())) {
					selectNode(node.getKey());
				} else {
					deselectNode(node.getKey());
				}
				break;
			}
		}
	}

	public void connectSelectedNodes() {
		for (Integer node1 : selectedNodes) {
			for (Integer node2 : selectedNodes) {
				super.connectNodes(node1, node2);
			}
		}
	}

	public void disconnectSelectedNodes() {
		for (Integer node1 : selectedNodes) {
			for (Integer node2 : selectedNodes) {
				super.disconnectNodes(node1, node2);
			}
		}
	}

	public Integer nodeClicked(Point pointClicked) {
		Integer nearestNode = null;
		for (Entry<Integer, Point> node : layout.entrySet()) {
			if (pointClicked.distance(node.getValue()) < radius / 2) {
				nearestNode = node.getKey();
			}
		}
		return nearestNode;
	}

	public Point giveViewRect() {
		int maxX = 0;
		int maxY = 0;
		for (Entry<Integer, Point> node : layout.entrySet()) {
			if (maxX < node.getValue().x) {
				maxX = node.getValue().x;
			}
			if (maxY < node.getValue().y) {
				maxY = node.getValue().y;
			}
		}
		return new Point(maxX + 4 * radius, maxY + 4 * radius);
	}

	public void drawPath(ArrayList<Integer> path, Color color, Graphics2D graphics) {
		if (path.size() < 2) {
			return;
		}
		Color previousColor = graphics.getColor();
		graphics.setColor(color);
		for (int i = 0; i < path.size() - 1; i++) {
			graphics.drawLine(layout.get(path.get(i)).x, layout.get(path.get(i)).y, layout.get(path.get(i + 1)).x, layout.get(path.get(i + 1)).y);
		}
		graphics.setColor(previousColor);
	}

	public void updateCurrentPath(ArrayList<Integer> currentPath) {
		this.currentPath = new ArrayList<>(currentPath);
		drawingPanel.repaint();
	}

	public boolean startPathFindingMainThread() {
		if (pathFinder != null && !pathFinder.isDone()) {
			pathFinder.cancel(true);
		}
		if (getStartNode() == null || getFinishNode() == null) {
			return false;
		}
		pathFinder = new SwingWorker<ArrayList<Integer>, ArrayList<Integer>>() {

			@Override
			protected ArrayList<Integer> doInBackground() {
				return findShortestPathThread();
			}

			@Override
			protected void process(List<ArrayList<Integer>> currentPaths) {
				updateCurrentPath(currentPaths.get(currentPaths.size() - 1));
				drawingPanel.repaint();
			}

			@Override
			public void done() {
				try {
					if (!killPathFinding) {
						shortestPath = get();
					}
					drawingPanel.repaint();
				} catch (InterruptedException | ExecutionException ex) {
					Logger.getLogger(GraphSwing.class.getName()).log(Level.SEVERE, null, ex);
				} finally {
					if (frame != null) {
						frame.enableGraphManagement();
					}
					if (killPathFinding || this.isCancelled()) {
						currentPath = null;
						shortestPath = null;
						drawingPanel.repaint();
						killPathFinding = false;
					}
				}
			}

			public ArrayList<Integer> findShortestPathThread() {
				ArrayList<Integer> path = null;
				int pathLength = Integer.MAX_VALUE;
				Stepper stepper = new Stepper(nodes, includedNodes, excludedNodes, getStartNode(), getFinishNode());
				while (stepper.stackNotEmpty() && !killPathFinding) {
					if (stepper.giveCurrentLength() < pathLength) {
						if (stepper.tryStepForward()) {
							if (stepper.pathFound()) {
								path = stepper.giveCurrentPath();
								pathLength = stepper.giveCurrentLength();
								stepper.stepBack();
								stepper.stepBack();
							} else if (stepper.cameToFinish()) {
								stepper.stepBack();
							}
						} else {
							stepper.stepBack();
						}
					} else {
						stepper.stepBack();
					}
					publish(stepper.giveCurrentPath());
					if (findSlowly) {
						try {
							Thread.sleep(500);
						} catch (InterruptedException ex) {
							Logger.getLogger(GraphSwing.class.getName()).log(Level.SEVERE, null, ex);
						}
					}
					while (paused) {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException ex) {
							Logger.getLogger(GraphSwing.class.getName()).log(Level.SEVERE, null, ex);
						}
						if (isCancelled()) {
							resumePathFinding();
							return null;
						}
					}
				}

				if (!killPathFinding) {
					return path;
				} else {
					return null;
				}
			}

		};
		if (frame != null) {
			frame.disableGraphManagement();
		}
		pathFinder.execute();
		return true;
	}

	@Override
	public boolean saveToFile(String filePath) {
                while (filePath.contains(".graph")){
                    filePath = filePath.replace(".graph", "");
                }
		try (FileOutputStream fileOut = new FileOutputStream(filePath + ".graph");
						ObjectOutputStream outStream = new ObjectOutputStream(fileOut)) {
			outStream.writeObject(nodes);
			outStream.writeObject(layout);
		} catch (IOException ex) {
			return false;
		}
		return true;
	}

	@Override
	public boolean loadFromFile(String filePath) {
		try (FileInputStream fileIn = new FileInputStream(filePath);
						ObjectInputStream inStream = new ObjectInputStream(fileIn)) {
			nodes = (Map<Integer, HashSet<Integer>>) inStream.readObject();
			layout = (Map<Integer, Point>) inStream.readObject();
		} catch (IOException | ClassNotFoundException ex) {
			return false;
		}
		return true;
	}

	public void setFrame(GraphManagable frame) {
		this.frame = frame;
	}

	public void unsetFrame() {
		this.frame = null;
	}

}
