package graph;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

class Stepper {

	private final Map<Integer, HashSet<Integer>> nodes;
	private final Set<Integer> includedNodes;
	private final Set<Integer> excludedNodes;

	private final List<Integer> stack;
	private final List<Iterator> iterators = new ArrayList<>();

	private final Integer startNode;
	private final Integer finishNode;

	public Stepper(Map<Integer, HashSet<Integer>> nodes, Set<Integer> includedNodes, Set<Integer> excludedNodes, Integer startNode, Integer finishNode) {
		this.nodes = Collections.unmodifiableMap(nodes);
		this.includedNodes = Collections.unmodifiableSet(includedNodes);
		this.excludedNodes = Collections.unmodifiableSet(excludedNodes);
		this.stack = new ArrayList<>(nodes.size());
		this.stack.add(startNode);
		this.iterators.add(this.nodes.get(startNode).iterator());
		this.startNode = startNode;
		this.finishNode = finishNode;
	}

	public ArrayList<Integer> giveCurrentPath() {
		ArrayList arrStack = (ArrayList) stack;
		return (ArrayList<Integer>) arrStack.clone();
	}

	public int giveCurrentLength() {
		return stack.size();
	}

	public Boolean pathFound() {
		return (stack.containsAll(includedNodes) && stack.get(stack.size() - 1).equals(finishNode));
	}

	public Boolean cameToFinish() {
		return stack.get(stack.size() - 1).equals(finishNode);
	}

	public Boolean tryStepForward() {
		Integer currentNode = stack.get(stack.size() - 1);
		Integer nextNode = currentNode;

		while (iterators.get(iterators.size() - 1).hasNext()
						&& (stack.contains(nextNode) || excludedNodes.contains(nextNode))) {
			nextNode = (Integer) iterators.get(iterators.size() - 1).next();
		}
		if (!stack.contains(nextNode)) {
			iterators.add(nodes.get(nextNode).iterator());
			stack.add(nextNode);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	public void stepBack() {
		if (stack.size() > 0) {
			iterators.remove(iterators.size() - 1);
			stack.remove(stack.size() - 1);
		}
	}

	public Boolean stackNotEmpty() {
		return stack.size() > 0;
	}

}

public class Graph {

	protected Map<Integer, HashSet<Integer>> nodes = new HashMap<>();

	protected Set<Integer> includedNodes = new HashSet<>();

	protected Set<Integer> excludedNodes = new HashSet<>();

	private Integer startNode = null;

	private Integer finishNode = null;

	public Integer getStartNode() {
		Integer safeStartNode = startNode;
		return safeStartNode;
	}

	public void setStartNode(Integer startNode) {
		this.startNode = startNode;
		includedNodes.remove(startNode);
		excludedNodes.remove(startNode);
		if (finishNode != null && finishNode.equals(startNode)) {
			finishNode = null;
		}
		includedNodes.remove(startNode);
		excludedNodes.remove(startNode);
	}

	public Integer getFinishNode() {
		Integer safeFinishNode = finishNode;
		return safeFinishNode;
	}

	public void setFinishNode(Integer finishNode) {
		this.finishNode = finishNode;
		includedNodes.remove(finishNode);
		excludedNodes.remove(finishNode);
		if (startNode != null && startNode.equals(finishNode)) {
			startNode = null;
		}
		includedNodes.remove(finishNode);
		excludedNodes.remove(finishNode);
	}

	public void addToIncludedNodes(Integer node) {
		includedNodes.add(node);
		excludedNodes.remove(node);
	}

	public void addToExcludedNodes(Integer node) {
		excludedNodes.add(node);
		includedNodes.remove(node);
	}

	public void removeFromIncludedNodes(Integer node) {
		includedNodes.remove(node);
	}

	public void removeFromExcludedNodes(Integer node) {
		excludedNodes.remove(node);
	}

	public void createNode(Integer node) throws CreatingAlreadyExistingNodeException {
		if (nodes.containsKey(node)) {
			throw new CreatingAlreadyExistingNodeException(node);
		} else {
			nodes.put(node, new HashSet<>());
		}
	}

	public void deleteNode(Integer node) throws NodeDoesNotExistException {
		if (nodes.containsKey(node)) {
			for (Integer connectedNode : nodesConnectedToNode(node)) {
				disconnectNodes(node, (Integer) connectedNode);
			}
			nodes.remove(node);
		} else {
			throw new NodeDoesNotExistException(node);
		}
	}

	public Set<Integer> nodesConnectedToNode(Integer node) {
		return (Set<Integer>) nodes.get(node).clone();
	}

	public boolean areConnected(Integer node1, Integer node2) throws NodeDoesNotExistException {
		if (!nodes.containsKey(node1)) {
			throw new NodeDoesNotExistException(node1);
		}
		if (!nodes.containsKey(node2)) {
			throw new NodeDoesNotExistException(node2);
		}
		if (nodes.get(node1).contains(node2)) {
			return true;
		}
		return false;
	}

	public void connectNodes(Integer node1, Integer node2) throws NodeDoesNotExistException {
		Set node1Set = nodes.get(node1);
		Set node2Set = nodes.get(node2);
		if (!nodes.containsKey(node1)) {
			throw new NodeDoesNotExistException(node1);
		}
		if (!nodes.containsKey(node2)) {
			throw new NodeDoesNotExistException(node2);
		}
		if (node1Set == null) {
			node1Set = new TreeSet<>();
		}
		if (node2Set == null) {
			node2Set = new TreeSet<>();
		}

		node1Set.add(node2);
		node2Set.add(node1);

	}

	public void connectSeveralNodes(Integer... nodes) throws NodeDoesNotExistException {
		for (int i = 0; i < nodes.length; i++) {
			Set nodeSet = this.nodes.get(nodes[i]);
			if (nodeSet == null) {
				throw new NodeDoesNotExistException(nodes[i]);
			}
			for (int j = 0; j < nodes.length; j++) {
				if (j == i) {
					continue;
				}
				if (!this.nodes.containsKey(nodes[j])) {
					throw new NodeDoesNotExistException(nodes[j]);
				}
				nodeSet.add(nodes[j]);
			}
		}
	}

	public void disconnectNodes(Integer node1, Integer node2) throws NodeDoesNotExistException {
		Set node1Set = nodes.get(node1);
		Set node2Set = nodes.get(node2);
		if (node1Set == null) {
			throw new NodeDoesNotExistException(node1);
		}
		if (node2Set == null) {
			throw new NodeDoesNotExistException(node2);
		}

		if (node1Set != null && node1Set.contains(node2)) {
			node1Set.remove(node2);
		}
		if (node2Set != null && node2Set.contains(node1)) {
			node2Set.remove(node1);
		}
	}

	public ArrayList<Integer> findShortestPath(Integer node1, Integer node2) {
		ArrayList<Integer> stack = new ArrayList<>(nodes.size());
		ArrayList<Integer> path = null;
		Integer currentNode = node1;
		Integer nextNode;
		ArrayList<Iterator> iterators = new ArrayList<>();
		int pathLength = Integer.MAX_VALUE;

		stack.add(node1);
		iterators.add(nodes.get(node1).iterator());
		do {
			nextNode = currentNode;
			while (iterators.get(iterators.size() - 1).hasNext() && stack.contains(nextNode)) {
				nextNode = (Integer) iterators.get(iterators.size() - 1).next();
			}
			if (stack.size() > pathLength || !iterators.get(iterators.size() - 1).hasNext() && stack.contains(nextNode)) {
				stack.remove(stack.size() - 1);
				iterators.remove(iterators.size() - 1);
				if (stack.size() > 0) {
					currentNode = stack.get(stack.size() - 1);
				} else {
					break;
				}
			} else {
				currentNode = nextNode;
				iterators.add(nodes.get(currentNode).iterator());
				stack.add(currentNode);
				if (node2.equals(stack.get(stack.size() - 1)) && (path == null || stack.size() < path.size())) {
					path = (ArrayList<Integer>) stack.clone();
					pathLength = path.size();
				}
			}
		} while (stack.size() > 0);
		return path;
	}

	public void generateGraph(int amountOfNodes, double density) {
		Random rand = new Random();
		int randomNum;
		int connectionsPerNode = (int) ((double) amountOfNodes * density);
		nodes = new HashMap<>(amountOfNodes);
		for (int i = 0; i < amountOfNodes; i++) {
			createNode(i);
		}
		for (int i = 0; i < amountOfNodes; i++) {
			for (int j = 0; j < connectionsPerNode; j++) {
				randomNum = rand.nextInt(amountOfNodes);
				while (randomNum == i) {
					randomNum = rand.nextInt(amountOfNodes);
				}
				connectNodes(randomNum, i);
			}
		}

	}

	public void generateFullGraph(int amountOfNodes) {
		nodes = new HashMap<>(amountOfNodes);
		for (int i = 0; i < amountOfNodes; i++) {
			createNode(i);
		}
		for (int i = 0; i < amountOfNodes; i++) {
			for (int j = 0; j < amountOfNodes; j++) {
				if (i != j) {
					connectNodes(i, j);
				}
			}
		}

	}

	public boolean saveToFile(String filePath) {
		try (FileOutputStream fileOut = new FileOutputStream(filePath);
						ObjectOutputStream outStream = new ObjectOutputStream(fileOut)) {
			outStream.writeObject(nodes);
		} catch (IOException ex) {
			return false;
		}
		return true;
	}

	public boolean loadFromFile(String filePath) {
		try (FileInputStream fileIn = new FileInputStream(filePath);
						ObjectInputStream inStream = new ObjectInputStream(fileIn)) {
			nodes = (Map<Integer, HashSet<Integer>>) inStream.readObject();
		} catch (IOException | ClassNotFoundException ex) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Graph{" + "nodes=" + nodes + '}';
	}
}
