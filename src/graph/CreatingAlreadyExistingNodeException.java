package graph;

public class CreatingAlreadyExistingNodeException extends RuntimeException{

    public CreatingAlreadyExistingNodeException(Integer node) {
        super(node.toString());
    }
    
}
