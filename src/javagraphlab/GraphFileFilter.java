/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javagraphlab;

import java.io.File;

/**
 *
 * @author Insider
 */
public class GraphFileFilter extends javax.swing.filechooser.FileFilter {
        @Override
        public boolean accept(File file) {
            // Allow only directories, or files with ".txt" extension
            return  file.isDirectory() || file.getAbsolutePath().endsWith(".graph");
        }
        @Override
        public String getDescription() {
            // Should use I18N?
            return "Graph documents (*.graph)";
        }

}
